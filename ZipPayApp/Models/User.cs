﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ZipPayApp.Models
{
    public class User
    {
        public int Id { get; set; }
        public string Name { get; set; }
        [EmailAddress]
        public string Email { get; set; }
        [Range(0.0, double.PositiveInfinity)]
        public decimal MonthlySalary { get; set; }
        [Range(0.0, double.PositiveInfinity)]
        public decimal MonthlyExpenses { get; set; }
    }
}
