﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ZipPayApp.DAL.Entities;

namespace ZipPayApp.DAL
{
    public class UnitOfWork : IDisposable
    {
        private ApplicationDbContext _context;

        public UnitOfWork(ApplicationDbContext context)
        {
            _context = context;
            UserRepository = new Repository<User>(context);
            AccountRepository = new Repository<Account>(context);
        }

        public Repository<User> UserRepository { get; private set; }
        public Repository<Account> AccountRepository { get; private set; }


        public void Save()
        {
            _context.SaveChanges();
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
