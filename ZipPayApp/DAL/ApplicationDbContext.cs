﻿using System;
using System.Collections.Generic;
using System.Linq;
using ZipPayApp.DAL.Entities;
using Microsoft.EntityFrameworkCore;

namespace ZipPayApp.DAL
{
    public class ApplicationDbContext : DbContext
    {
        public DbSet<User> Users { get; set; }
        public DbSet<Account> Accounts { get; set; }
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options)
        {
            Database.EnsureCreated();
        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<User>()
                .HasKey(u => u.Id); 
            modelBuilder.Entity<User>()
                .HasIndex(u => u.Email).IsUnique();

            modelBuilder.Entity<User>()
                .HasOne(u => u.Account)
                .WithOne(a => a.User)
                .HasForeignKey<Account>(a => a.UserId);

            modelBuilder.Entity<Account>()
                .HasKey(a => a.Id);
            //modelBuilder.Entity<Account>()
            //    .HasOne(a => a.User)
            //    .WithOne(u => u.Account)
            //    .HasForeignKey<>(p => p.)
            //    .IsRequired();
        }
    }
}
