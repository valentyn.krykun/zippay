﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ZipPayApp.DAL.Entities
{
    public class Account : Base
    {
        public int UserId { get; set; }
        public User User { get; set; }
        public string Description { get; set; }
    }
}
