﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Threading.Tasks;
using ZipPayApp.Models;
using ZipPayApp.Services;

namespace ZipPayApp.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class AccountsController : ControllerBase
    {
        private readonly ILogger<AccountsController> _logger;
        private readonly IAccountService _accountService;

        public AccountsController(ILogger<AccountsController> logger, IAccountService accountService)
        {
            _logger = logger;
            _accountService = accountService;
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<Account>>> Get()
        {
            return Ok(_accountService.GetAccounts());
        }

        [HttpPost]
        public async Task<ActionResult<Account>> Post(Account account)
        {
            if (account == null || !ModelState.IsValid)
            {
                return BadRequest();
            }
            var createdAccount = _accountService.CreateAccount(account);

            return Created(nameof(Account), createdAccount);
        }
    }
}
