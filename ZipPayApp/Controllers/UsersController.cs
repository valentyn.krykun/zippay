﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ZipPayApp.Models;
using ZipPayApp.Services;

namespace ZipPayApp.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class UsersController : ControllerBase
    {
        private readonly ILogger<UsersController> _logger;
        private readonly IUserService _userService;

        public UsersController(ILogger<UsersController> logger, IUserService userService)
        {
            _logger = logger;
            _userService = userService;
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<User>> Get(int id)
        {
            var user = _userService.GetUser(id);
            if (user == null)
            {
                return NotFound(id);
            }
            return Ok(user);
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<User>>> Get()
        {
            return Ok(_userService.GetUsers());
        }

        [HttpPost]
        public async Task<ActionResult<User>> Post(User user)
        {
            if (user == null || !ModelState.IsValid)
            {
                return BadRequest();
            }
            var createdUser = _userService.CreateUser(user);

            return Created(nameof(User), createdUser);
        }
    }
}
