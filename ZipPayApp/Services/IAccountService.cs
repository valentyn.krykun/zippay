﻿using System.Collections.Generic;
using ZipPayApp.Models;

namespace ZipPayApp.Services
{
    public interface IAccountService
    {
        IEnumerable<Account> GetAccounts();
        Account CreateAccount(Account account);
    }
}
