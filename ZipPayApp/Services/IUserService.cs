﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ZipPayApp.Models;

namespace ZipPayApp.Services
{
    public interface IUserService
    {
        IEnumerable<User> GetUsers();
        User GetUser(int id);
        User CreateUser(User user);
        decimal GetUserBalance(User user);
    }
}
