﻿using AutoMapper;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using ZipPayApp.DAL;
using ZipPayApp.Models;

namespace ZipPayApp.Services
{
    public class AccountService : IAccountService
    {
        private readonly ApplicationDbContext _context;
        private readonly IMapper _mapper;
        private readonly IUserService _userService;
        private readonly ILogger<AccountService> _logger;

        public AccountService(ApplicationDbContext context, IMapper mapper, IUserService userService, ILogger<AccountService> logger)
        {
            _context = context;
            _mapper = mapper;
            _userService = userService;
            _logger = logger;
        }

        public Account CreateAccount(Account account)
        {
            if (account == null)
            {
                throw new ArgumentNullException(nameof(Account));
            }
            var user = _userService.GetUser(account.UserId);
            if (user == null)
            {
                throw new InvalidOperationException($"User with id {account.UserId} is not exist");
            }
            if (_userService.GetUserBalance(user) < 1000)
            {
                throw new InvalidOperationException("User balance cannot be less than 1000");
            }

            UnitOfWork unitOfWork = new UnitOfWork(_context);

            var insertedAccount = unitOfWork.AccountRepository.Insert(_mapper.Map<DAL.Entities.Account>(account));
            unitOfWork.Save();

            account.Id = insertedAccount.Id;
            return account;
        }

        public IEnumerable<Account> GetAccounts()
        {
            UnitOfWork unitOfWork = new UnitOfWork(_context);

            var accounts = unitOfWork.AccountRepository.Get();

            return _mapper.Map<IEnumerable<Account>>(accounts);
        }
    }
}
