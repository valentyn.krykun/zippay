﻿using AutoMapper;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using ZipPayApp.DAL;
using ZipPayApp.Models;

namespace ZipPayApp.Services
{
    public class UserService : IUserService
    {
        private readonly ApplicationDbContext _context;
        private readonly IMapper _mapper;
        private readonly ILogger<UserService> _logger;

        public UserService(ApplicationDbContext context, IMapper mapper, ILogger<UserService> logger)
        {
            _context = context;
            _mapper = mapper;
            _logger = logger;
        }

        public User CreateUser(User user)
        {
            if (user == null)
            {
                throw new ArgumentNullException(nameof(User));
            }
            UnitOfWork unitOfWork = new UnitOfWork(_context);

            //this exception also can be thrown by DB later
            if (unitOfWork.UserRepository.Get(u => u.Email == user.Email).FirstOrDefault() != null)
            {
                throw new InvalidOperationException($"User with email {user.Email} already exists");
            }

            var insertedUser = unitOfWork.UserRepository.Insert(_mapper.Map<DAL.Entities.User>(user));
            unitOfWork.Save();

            user.Id = insertedUser.Id;
            return user;
        }

        public User GetUser(int id)
        {
            if (id <= 0)
            {
                throw new ArgumentException(nameof(User));
            }
            UnitOfWork unitOfWork = new UnitOfWork(_context);

            var user = unitOfWork.UserRepository.GetByID(id);

            return _mapper.Map<User>(user);
        }

        public IEnumerable<User> GetUsers()
        {
            UnitOfWork unitOfWork = new UnitOfWork(_context);

            var users = unitOfWork.UserRepository.Get();

            return _mapper.Map<IEnumerable<User>>(users);
        }

        public decimal GetUserBalance(User user)
        {
            if (user == null)
            {
                throw new ArgumentNullException(nameof(User));
            }
            return user.MonthlySalary - user.MonthlyExpenses;
        }
    }
}
